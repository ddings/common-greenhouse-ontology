# JSON &rarr; JSON-LD + YAML

Many systems nowadays have RestAPIs that output (and input) their data in JSON format. [JSON-LD](https://json-ld.org/) is a JSON-based data format for [Linked Data](https://en.wikipedia.org/wiki/Linked_data). It makes JSON data more interoperable across the web, and also more expressive. We can say that JSON-LD adds semantics or meaning to the data, compared to the JSON format. In addition, we can use JSON-LD to make data conform to standards such as ontologies, preserving human- and machine-readibility. [YAML](https://en.wikipedia.org/wiki/YAML) is a serialization language that is also human- and machine-readable. It is commonly used for configuration languages, i.e., it contains metadata. In our case, we can use YAML to write the schema of our JSON-LD document. In this way, the JSON-LD contains the data, while the YAML schema specifies the requirements of the JSON-LD and adds more information about the data, such as data formats and comments. This way, the YAML schema can be used by developers to understand better the contents of the JSON-LD document as well as by other systems to automatically validate the content of the JSON-LD. For more information about YAML and some examples, [see this post](https://www.redhat.com/en/topics/automation/what-is-yaml).

As an example, we have translated part of the JSON output of the system KIS ([see file](https://gitlab.com/ddings/common-greenhouse-ontology/-/blob/new_kis_jsonld/Applications/KIS/kis-test.json?ref_type=heads)) into its JSON-LD representation ([see file](https://gitlab.com/ddings/common-greenhouse-ontology/-/blob/new_kis_jsonld/Applications/KIS/kis-test.jsonld?ref_type=heads)) according to the Common Greenhouse Ontology (CGO). In particular, we have picked some keys from the `generalInformation` object in the KIS JSON, and all the keys in the object `geometryGenericDesign`, inside the object `geometryAndConstructionProject`, to show how this transformation can be done and what are the differences between the data in JSON and the data expressed in JSON-LD. Moreover, we have built an schema of this JSON-LD format, that can be used to annotate and validate it (similarly to a [JSON Schema](https://json-schema.org/overview/what-is-jsonschema)), as mentioned before. The schema is written in [YAML](https://yaml.org/) ([see file](https://gitlab.com/ddings/common-greenhouse-ontology/-/blob/new_kis_jsonld/Applications/KIS/kis-test.yaml?ref_type=heads)). Below we describe how this has been done.

### Advantages of using JSON-LD + YAML over JSON

While the JSON-LD format requires writing more lines of code compared to using JSON, there are several reasons why the effort is worthwile:
- The JSON-LD adds semantics. As demonstrated below, the JSON-LD describes important information about the data that makes it more meaningful. By only looking at the JSON-LD, developers can see, for exmaple, the units of the different numbers or that the key `width` refers to the bay width of the greenhouse.
- In the YAML file, the specific formats of the data and the structure of the JSON-LD are defined. We can specify that in our data, we are representing a number with a `string` instead of a `integer`. We can also define which keys are required. This adds clarity for developers, reducing mistakes in the code and bugs. In the long run, this increases not only the quality of the data, but also the quality of the code built around it, decreasing debugging times.
- Eventually, if all sytems comply to a standard like the CGO, this makes all these systems interoperable (i.e., they all speak the same language). In this scenario, data exchange accross these systems is a breeze.

## Step by step guide
0.  Define the `@context` and `@graph` sections in the JSON-LD. The `@context` maps the short keys used in the JSON data to their full URIs defined in the ontology. The `@graph` key in the JSON-LD is typically an array that contains multiple JSON-LD objects (nodes), each representing an entity. (For more information go to the [official documentation](https://www.w3.org/TR/json-ld11/#basic-concepts)).
1. Locate in the JSON the keys to be mapped into JSON-LD using the ontology.
2. Find the equivalent entity for a JSON key in the ontology. This step can be challenging and might require certain domain expertise. It is helpful to use [Protégé](https://protege.stanford.edu/) or [TopBraid](https://www.topquadrant.com/topbraid-edg/) to use their search functions and visualize the entities and their relations.
3. In JSON-LD, an ontology class is a JSON-LD object in which the property `@type` points at the ontology class. A class relates to other class (object property) or piece of data (data property) through the nested object structure in the JSON-LD. Thus, an object property is a JSON-LD object inside the domain class (in the example below a `cgo:VenloGreenhouse`), with its key indicating the name of the object property (in the example below `cgo:hasBayWidth`) and its property `@type` pointing at the range class (in the example below a `om:Width`). A data property is a JSON-LD object inside the domain class (in the example below a `om:hasValue`) with 2 properties: `@value` and `@type`.
4. The YAML describes the structure of the JSON-LD. Ontology classes are represented by objects with `type:object`. The YAML objects contain a property called `required`, which is an array that defines the required properties of the JSON-LD object. These required properties can be `@id`, `@type`, and all the object properties and data properties of that class in the JSON-LD according to the ontology definition. The YAML object also contains a property called `properties` which is an array. In this array the properties are defined. Data properties are directly defined, including the defintion of their `@value` and `@type` properties. Object properties are expressed as a reference with `cgo:hasSectionLength: $ref: "#/$defs/length"`. Thus, the domain is represented by the object in which this property is included and the range is defined later on as an object, in this case with key `length`.
5. Use [JSON-LD Playground](https://json-ld.org/playground/) and a [YAML validator](https://onlineyamltools.com/validate-yaml) to validate the JSON-LD and the YAML, respectively.
![alt text](kis-test-jsonld-steps.png)

### From JSON to JSON-LD

Following the CGO, we can see how the key `width` with value `4` in the JSON can be expressed using the property in the CGO called `cgo:hasBayWidth` that relates the class `cgo:Greenhouse` with the class `om:Width`. This gives us more information about this greenhouse property (such as the unit of measure), compared to the JSON. In a similar fashion, the key `bayspPerSpan` with value `2` is represented in the JSON-LD with an object with the key `cgo:hasNumberOfBaysPerSpan`, which relates the class `cgo:Greenhouse` with the class `om:Number`.

#### JSON:

```json
"geometryGenericDesign": {
    "width": 4.0,
    "baysPerSpan": 2
}
```

#### JSON-LD:

```json
{
  "@context": {
    "owl": "http://www.w3.org/2002/07/owl#",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "xsd": "http://www.w3.org/2001/XMLSchema#",
    "om": "http://www.ontology-of-units-of-measure.org/resource/om-2/",
    "cgo": "https://www.tno.nl/agrifood/ontology/common-greenhouse-ontology#",
    "kis": "https://example.org/kis-greenhouse-parameters#"
  },
  "@graph": [
    {
      "@id": "kis:Greenhouse_1",
      "@type": "cgo:VenloGreenhouse",
      "rdfs:label": "Greenhouse",
      "cgo:hasBayWidth": {
        "@id": "kis:BayWidth_1_Greenhouse_1",
        "@type": "om:Width",
        "om:hasValue": {
          "@id": "om:Measure_4.0",
          "@type": "om:Measure",
          "om:hasNumericalValue": {
            "@value": "4.0",
            "@type": "xsd:decimal"
          },
          "om:hasUnit": {
            "@type": "om:metre"
          }
        }
      },
      "cgo:hasNumberOfBaysPerSpan": {
        "@id": "kis:NumberOfBaysPerSpan_1_Greenhouse_1",
        "@type": "om:Number",
        "om:hasValue": {
          "@id": "om:Measure_2",
          "@type": "om:Measure",
          "om:hasNumericalValue": {
            "@value": "2",
            "@type": "xsd:integer"
          }
        }
      }
    }
  ]
}
```

As a less trivial example, the different greenhouse parts, that in the CGO can be represented as triples with: `cgo:Greenhouse` &rarr;`cgo:hasPart`&rarr;`cgo:GreenhouseConstruction` and `cgo:GreenhouseConstruction`&rarr;`cgo:hasPart`&rarr;`cgo:ConstructionPart` where `cgo:ConstructionPart` is the superclass for `cgo:Truss`, `cgo:Floor`, `cgo:Roof`, etc., are represented in the JSON-LD has an array with key `cgo:hasPart` of objects, in which each object is a `cgo:ConstructionPart`. This modeling decision makes the JSON-LD compliant with its specification (i.e., duplicate keys are not allowed), while trying to convey the same idea as in an ontology.

In the JSON below, we have the keys `roofAngle` and `roofGlass` with values `22.0` and `1125`. In the JSON-LD, the property `cgo:hasAngle` of the object represented by the CGO class `cgo:Roof` relates the `cgo:Roof` with the class `om:Angle`. The `roofGlass` is represented by the property `cgo:hasNominalGlassWidth` that relates the class `cgo:InnerfieldGlassPane` with the class `cgo:Width`. The `cgo:InnerfieldGlassPane` is a part of the `cgo:Roof` related by the property `cgo:hasPart`. By only looking at the data, now we know that `1125` is a measure in millimetres of the inner field glass pane that is part of the roof of a greenhouse.

#### JSON:

```json
"geometryGenericDesign": {
    "roofAngle": 22.0,
    "roofGlass": 1125,
}
```

#### JSON-LD:

```json
{
  "@context": {...},
  "@graph": [
    {
      "@id": "kis:Greenhouse_1",
      "@type": "cgo:VenloGreenhouse",
      "rdfs:label": "Greenhouse",
      "cgo:hasPart": {
        "@id": "kis:GreenhouseConstruction_1_Greenhouse_1",
        "@type": "cgo:GreenhouseConstruction",
        "cgo:hasPart": [
          {
            "@id": "kis:Roof_1_Greenhouse_1",
            "@type": "cgo:Roof",
            "cgo:hasAngle": {
              "@id": "kis:RoofAngle_1_Greenhouse_1",
              "@type": "om:Angle",
              "om:hasValue": {
                "@id": "om:Measure_22.0",
                "@type": "om:Measure",
                "om:hasNumericalValue": {
                  "@value": "22.0",
                  "@type": "xsd:decimal"
                },
                "om:hasUnit": {
                  "@type": "om:degree"
                }
              }
            },
            "cgo:hasPart": [
              {
                "@id": "kis:RoofGlass_1_Greenhouse_1",
                "@type": "cgo:InnerfieldGlassPane",
                "cgo:hasNominalGlassWidth": {
                  "@id": "kis:InnerfieldGlassPaneNominalGlassWidth_1_Greenhouse_1",
                  "@type": "om:Width",
                  "om:hasValue": {
                    "@id": "om:Measure_1125.0",
                    "@type": "om:Measure",
                    "om:hasNumericalValue": {
                      "@value": "1125.0",
                      "@type": "xsd:decimal"
                    },
                    "om:hasUnit": {
                      "@type": "om:millimetre"
                    }
                  }
                }
              },
              {
                "@id": "kis:RoofGlassEndZone_1_Greenhouse_1",
                "@type": "cgo:EndZoneGlassPane",
                "cgo:hasNominalGlassWidth": {
                  "@id": "kis:EndZoneGlassPaneNominalGlassWidth_1_Greenhouse_1",
                  "@type": "om:Width",
                  "om:hasValue": {
                    "@id": "om:563.0",
                    "@type": "om:Measure",
                    "om:hasNumericalValue": {
                      "@value": "563.0",
                      "@type": "xsd:decimal"
                    },
                    "om:hasUnit": {
                      "@type": "om:millimetre"
                    }
                  }
                }
              }
            ]
          },
          {... more cgo:ConstructionPart objects}
        ]
      }
    }
  ]
}
```

We can use the [JSON-LD Playground](https://json-ld.org/playground/) to validate, format, and visualize (see below) the JSON-LD data.
![alt text](kis-test-jsonld.png)

### Schema of the JSON-LD data in YAML

The schema defines the structure and rules of the JSON-LD data. An schema can be used to include metadata (e.g., informative comments) and to validate the data. Additionally, it could be used to automate the data transformations (e.g., from JSON to JSON-LD). With YAML, we can represent this in a lightweight format that is both human- and machine-readable.

We have represented the notion of triples in the CGO in the same way as described above for the JSON to JSON-LD transformation: using arrays. In the example below, the key `cgo:hasPart` represents an array in which each item is a reference to an object. Each object is defined independently in the same way as the `greenhouseConstruction` object below.

#### YAML:

```yaml
greenhouseConstruction:
  type: object
  required:
    - "@id"
    - "@type"
    - cgo:hasPart
  properties:
    "@id":
      type: string
      format: iri
    "@type":
      const: cgo:GreenhouseConstruction
    cgo:hasPart:
      - $ref: "#/$defs/truss"
      - $ref: "#/$defs/perimeterFoundation"
      - $ref: "#/$defs/roof"
      - $ref: "#/$defs/gableEnd"
      - $ref: "#/$defs/sideWall"
  "$comment": Physical greenhouse building and its different parts.
```

## Automating the process
With libraries such as [RDFLib](https://rdflib.readthedocs.io/en/stable/) we can parse a knowledge graph or ontology in Turtle (.ttl) into a JSON-LD serialization file. And from that file, we could extract when needed other JSON-LD subfiles, one for each message or RestAPI GET/POST interaction for the systems exchanging data. This would greatly automate the step towards a JSON-LD file. As mentioned above, the YAML file could also be used to automate the transformation process. This is work in progress.

## JSON-LD client libraries
Once a GET/POST operation has been defined to provide JSON-LD data according to the YAML schema, the RestAPI can be made available at some endpoint. Requesting systems can then use the GET/POST operations to retrieve JSON-LD formatted data that complies to the defined YAML schema. Processing and parsing this JSON-LD data is then a next step. Work in progress also includes the development of a library or package that can be used for this processing and parsing process. Some existing libraries such as [rdflib.js](https://github.com/linkeddata/rdflib.js) or [jsonld.js](https://github.com/digitalbazaar/jsonld.js/?tab=readme-ov-file) can help with manipulating JSON-LD data in client applications, in this case written in JavaScript.