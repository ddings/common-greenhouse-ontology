# Common Greenhouse Ontology

This repository contains the Common Greenhouse Ontology (CGO) and the documentation about the ontology.

## CGO description

Greenhouses are used to grow vegetables and plants year-round. The entire eco-system in and around the
greenhouse needs to be as optimal as possible to grow crop as efficient as possible with minimal use of
resources. During the phases of design, construction, maintenance and operation of a greenhouse
various types of data are generated by and exchanged between different stakeholders, such as
construction companies, suppliers of installations and control systems, growers and advisors.

This data is being stored in different systems with differing format and meaning. A common language 
with unambiguous semantics is therefore necessary to exchange this data, understand it and
process it for further usage. The goal of the Common Greenhouse Ontology (CGO) is to be this
common language that can be used between all these different systems. It provides semantic alignment
of different systems and needs to considered as a standard on data of high-tech greenhouses.

The first version of the CGO was created in 2019 as part of a Dutch national project
Data-Driven Integrated Greenhouse Systems (DDINGS). In this project, a proof-of-concept datahub was
created to connect different greenhouse systems and applications [1]. The first version of the CGO
was introduced to standardize the definitions of operational data about the climate in the
greenhouse and the installations, sensors and control systems to deal with this. Thereby, the CGO
reused some existing ontologies, such as the Semantic Sensor Network ontology [2] and the Ontology
of units of Measure [3].

In the meantime, subsequent versions have been produced to cover also construction concepts,
robotic systems and objects used in a Greenhouse.


# Current stable version V2.4

The current stable version is V2.4. A .ttl representation of the ontology can be found in file
CGO-v2.4-current-stable-version.ttl in folder [Ontologies](./Ontologies).


## Change log for V2.4 with respect to V2.3

With respect to the previous version V2.3 of the CGO, concepts have been added in three application areas:

1: The application area of greenhouse control, concepts have been added to cover:
- simulation and calculation of a cultivation in a greenhouse
- cultivation period properties, such as start date, end date and day topping
- observations of plant development parameters, such as leaf area index and stem density
- observations of specific PAR generated by artificla illumination and its unit of measure

2: The application area of robotics, concepts have been added to cover:
- diseases and pests on fruits and vegetables managed by robots
- dents and bruices on produce harvested in the greenhouse by robots
- harvesting and collecting in crates and delivering to a sorting robot

3: The application area of greenhouse construction, changes were made to:
- add or improve on the definitions of various construction concepts and properties

In addition, a complete new folder named Applications has been added that shows how the CGO can be applied
to these application areas. There is a special focus on how to get from the CGO to JSON-LD specifications
of messages or operations to be used on RestAPI endpoint of existing or new greenhouse systems.
See [here](https://gitlab.com/ddings/common-greenhouse-ontology/-/tree/main/Applications).

## Change log for V2.3 with respect to V2.2

With respect to the previous version V2.2 of the CGO, concepts have been added in two application areas:

1: The application area of robotics, concepts have been added to cover:
- moving objects in the greenhouse, such as robots or humans
- movable objects in the greenhouse that can be moved by humans or robots, e.g. tools, plants etc.
- multiple roles of humans in the greenhouse
- classifications of unexpected situations detected by a robot
- severity and certainty levels of a detected obstacle and situation
- messages to be transferred to the operator, like info, warning or alarm.
A complete list of all added classes and properties can be found in the file SENS-CGO-extensions.ttl in
folder [Ontologies/Older versions](./Ontologies/Older versions).

2: The application area of greenhouse construction, concepts have been added to cover:
- general information about a contractor for greenhouse construction
- generic geometry data for a greenhosue to be constructed, such as baywidth, column height, section length
- innerfield dimensions and partitions inside the greenhouse
- ground and foundation properties
- live loads of the greenhouse, such as roof dead load, wind load, snow load, gutter and truss weight
- crop wire system and loads for services and maintenance
- various parts of the greenhouse, such as gable end, side wall, gutter and rain water system


# Development method

The CGO was developed with domain experts and focuses on greenhouse-related concepts and measurements. 
Several ontology development methods were consulted and applied at various stages of development, 
including SABiO [4], and Ontology 101 [5]. The CGO also makes use of other ontologies: 
the Semantic Sensor Network ontology (SSN), which includes the Sensor, Observation, Sample, 
and Actuator ontology (SOSA), and the Ontology of units of Measure (OM).

## Observations and measurements in CGO

Observations are an important aspect of the CGO. In the CGO, the SOSA ontology is used to
model observations. In Figure 1, a simplified version of SOSA’s architecture of observations is shown
in green. The example observation is the thickness of a stem (of a flower). The stem is the part that is
observed, the Feature of Interest. The property we are observing is its thickness, the Observable
Property. The Feature of Interest and the Observable Property, but also the result, are linked to the
Observation, the center of the architecture to which all elements are connected.

![img.png](img.png)

The Ontology of units of Measure (OM) is an OWL ontology for the domain of quantities,
measurements and units of measure [3]. OM provides the possibility to link aspects of an
observation to a quantity and a unit. A schematic overview of how this is implemented in CGO is
given in Figure 1. From a cgo:Class (e.g. thickness), a link to an om:Quantity (e.g. om:Width) can be
provided. The results are linked to a numerical value through om:NumericalValue (this is not shown
in Figure 1 for reasons of simplification) and to a unit through om:hasUnit.

## CGO main classes

The CGO contains hundreds of classes, properties (both data and object properties), and
individuals [1]. This includes the classes and properties from the SOSA ontology. We can divide the
content into four main categories: the greenhouse, which is the center concept of the ontology,
features, which are set properties of the greenhouse such as its dimensions, parts, which are the
objects that can be found in greenhouses, and finally, measurements. The measurements in the greenhouse are modelled 
using SOSA [2] and OM [3] as could be seen in Figure 1. SOSA classes are
connected to CGO classes by being superclasses of possible Features of Interest and Observable
Properties, OM classes are connected to data properties of CGO and as a subtype of sosa:Result.
Each of the categories contain a variety of classes. The parts category contains over 150 classes
and provides an elaborate, yet not complete, overview of parts of a high-tech greenhouse. Important
subsets are systems and construction hierarchies. The systems subset describes a wide set of systems
in a greenhouse ranging from broader ventilation systems to specific geothermal heat pumps. The
construction subset includes classes such as screens and ventilation vents. These classes are all
connected to the center of CGO, the greenhouse class, by the object property part of. As mentioned
above, measurements are modelled through SOSA, but features of the greenhouse such as its
orientation and location are expressed with data properties.

## Competency questions and SHACL constraints

For testing the completeness of the CGO, we created competency questions (CQ) in collaboration
with greenhouse experts. For querying, SPARQL queries were made from these CQ's, and restrictions were expressed using SHACL
constraints.

## Visualizing the ontology

A visualisation of the ontology gives a quick overview and can add to the understanding of the design choices 
described above. For displaying the ontology, first download the ontology to your device. Then go to the following link:

[webvowl semantic treehouse](https://webvowl.semantic-treehouse.nl/)

Upload the ontology by going to the tab ***Ontology*** in the toolbar at the bottom of your screen. 
 Click ***select ontology file***, and select the CGO ontology.  

For editing the ontology for reuse we recommend using Protégé or Topbraid.


# Contributing to the CGO

If you want to make changes to the ontology or other content in this repository, please read the [CGO manual](DDINGS%20Manual%20CGO.pdf)
and this readme first. If you want to make small changes:
1. checkout the dev branch 
2. bring it up to date with the master 
3. make your changes and document them in a commit message 
4. open a new merge request for merging your changes in the master 
5. One of the maintainers will approve the changes and merge 

For larger changes **create a new branch** and regularly commit with descriptive commit messages. If your changes are 
related to an issue, use #issuenumber (e.g. #7) to tag your issue in the commit message.

## Documentation

For more development information on how to find and add concepts to the CGO, checkout the [CGO manual](DDINGS%20Manual%20CGO.pdf). 
For a more elaborate theoretical description, read our paper on the CGO (submitted) [7].


# Team

More information about the CGO and its applications, contact Jack Verhoosel (jack.verhoosel@tno.nl)

## Ontology engineers
Jack Verhoosel, TNO  
Cornelis Bouter, TNO  
Julia Garcia Fernandez, TNO  
Klaas Andries de Graaf, TNO  
Roos Bakker, TNO  
Romy van Drie, TNO  
Barry Nouwt, TNO  
Han Kruiger, TNO  
Ellen van Bergen, TNO  
Sander van Leeuwen, WUR  
Lorijn van Rooijen, WUR  

## Greenhouse domain experts
Athanasios Sapounas, TNO  
Joris van Duijneveldt, TNO  
Bart Slager, TNO

## Advisors
Jan Top, WUR


# References

1. Verhoosel, J.P.C.; Nouwt, B.; Bakker, R.M.; Sapounas, A.; Slager, B. A Datahub for Semantic Interoperability
in Data-Driven Integrated Greenhouse Systems. EFITA conference 2019.
2. Neuhaus, H.; Compton, M. The semantic sensor network ontology. AGILE workshop on challenges in geospatial
data harmonisation, Hannover, Germany. 2009.
3. Rijgerberg, H.; Wigham, M.L.I.; Top, J.L. How semantics can improve engineering processes: A case of units
of measure and quantities. Adv. Eng. Inform 2011, 25, 276-287.
4. de Almeida Falbo, R. Experiences in using a method for building domain ontologies. The 16th International
Conference on Software Engineering and Knowledge Engineering, SEKE. 2004.
5. Noy, N.; McGuinness, D. Ontology development 101: A guide to creating your first ontology. Development
2001, 32, 1-25.
6. Bouter, C.; Kruiger, H.; Verhoosel, J. Domain-Independent Data Processing in an Ontology Based Data
Access Environment using the SOSA Ontology, Demonstration track of the 2021 Formal Ontology in
Informations Systems Conference, 2021, Submitted.
7. Bakker, R.M.; van Drie, R.A.N.; Bouter, C.A.; van Leeuwen, S.; van Rooijen, L.; Top, J. The Common Greenhouse Ontology: an ontology
describing components, properties, and measurements inside the greenhouse. EFITA conference 2021, submitted


# License 

CGO is released under Apache 2.0, for more information see the LICENSE.
